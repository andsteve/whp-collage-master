
var app = angular.module('teamCollage',['ngRoute']);

/**
* This is to configure the route for the application
*/
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {templateUrl : "views/partials/prodigious_team.html", controller: "teamCollageController"})
    .when("/admin/counts", {templateUrl : "views/partials/counts.html",controller:'countsController'})
    .when("/admin/:id",{templateUrl : "views/partials/big_count.html",controller:'bigCountController'})
    .when("/admin/mark/:mark",{templateUrl : "views/partials/mark.html",controller:'markController'})
    .otherwise({
    	redirectTo: "/"
    });
});

/**
* This controller initialize the aplication
*/
// app.controller('initController', ['$scope', function($scope) {
// 	init();
// }]);

app.controller('countsController', ['$scope', function($scope) {

}]);

app.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();

                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;

                    });
                }
               // scope.tmppath= URL.createObjectURL(event.target.files[0]);

               // console.log(tmppath);
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);

app.controller('bigCountController', ['$scope','$routeParams','$http','$location', function($scope,$routeParams,$http,$location) {

	/* this is to be sure that there are parameter in the url if not go home */
	if(Object.getOwnPropertyNames($routeParams).length <= 0){
		
		$location.path("/");
		
		/*this return is for stop the propagation when the there are not parameters*/
		return false;	
	}
	/* this variable save the id that the user select*/
	$scope.countId = $routeParams.id;
	$scope.test ={};

	
	/**
	* This function searches, which big-count does match with the id,
	*/
	$scope.findBigCount = function(){

		for (var i = 0; i < $scope.bigCountsList.length; i++) {

			if ($scope.countId === $scope.bigCountsList[i].id) {
				$scope.bigCount = $scope.bigCountsList[i];
				break;
			}
		}
		$scope.LoadNameTeams($scope.bigCount);
	}

	/**
	* If the paratemer in the url is correct this function loads the json teams and save 
	* all teams name on a variable but if not correct return home
	*/
	$scope.LoadNameTeams = function(bigCount){
		if(bigCount){
			$http.get('json/teams.json').then(function(data){

				$scope.teams = data.data[bigCount.name];
				$scope.listenMarkClick();
			});
		}else{
			$location.path("/");
		}
	}

	/**
	* This function catch the mark or team that was clicked
	*/	
	$scope.listenMarkClick = function (){
		var marks = document.getElementsByClassName('marks')[0];
		
		marks.addEventListener("click", findMarkClass, false); 
		
		/**
		* This function search if the item selected has the class 'mark'
		*/		
		function findMarkClass(e){

			var mark = '';
			if(e.target.classList.contains("mark")){
				
				mark = e.target.getElementsByTagName("h2")[0].innerHTML;
				$scope.searchTeam(mark);
			}
			else{

				if (e.target.parentElement.classList.contains("mark")) {
					mark = e.target.innerHTML;
					$scope.searchTeam(mark);
				}
			}
		}

		/**
		* This function is for admin part, this are going to load the team 
		* that was selected
		*/
		$scope.searchTeam = function(teamSelected){
		    $http.get('json/teams/'+teamSelected+'.json').then(function(data){

		      	$location.path("/admin/mark/"+teamSelected)

		    });
		}
	}

	$scope.findBigCount();


	/**
	*/
	$scope.modalEvent = function(){
		// Get the modal
		var modal = document.getElementById('myModal');

		// Get the button that opens the modal
		var btn = document.getElementById("myBtn");

		// Get the <span> element that closes the modal
		var span = document.getElementsByClassName("close")[0];

		// When the user clicks on the button, open the modal 
		btn.onclick = function(e) {
		    modal.style.display = "block";
		}

		// When the user clicks on <span> (x), close the modal
		span.onclick = function() {
		    modal.style.display = "none";
		}

		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
		    if (event.target == modal) {
		        // modal.style.display = "none";
		    }
		}
	}
	$scope.modalEvent();



	$scope.onSubmit = function(e){

			// var tmppath = URL.createObjectURL(e.target.files[0]);
              console.log($scope.tmppath);

		    // var x = document.getElementById("myFile").value;
		    // document.getElementById("demo").innerHTML = x;
		
		// console.log(x);
	}


}]);

app.controller('markController', ['$scope','$routeParams','$http', function($scope,$routeParams,$http) {	

	$http.get('json/teams/'+$routeParams.mark+'.json').then(function(data){
		$scope.mark = data.data;
	});

	$scope.enable = function(id){
		
		var tagTr = document.getElementById(id);

		var  checkBox = tagTr.getElementsByTagName("input")[0];
		var buttons = tagTr.querySelectorAll("button");
		var inputs = tagTr.querySelectorAll('input[type="text"]');
		
		var isChecked = checkBox.checked;

		/* it checks if the checkbox is selected or not */
		if(isChecked){
			isChecked = false;
		}else{
			isChecked = true;
		}

		for (var i = 0; i < inputs.length; i++) {
			/* this is to be enable or disable the buttons*/
			if(i <= 1){
				buttons[i].disabled = isChecked;
			}
			/* this is to be enable or disable the inputs*/
			inputs[i].disabled = isChecked;
		}

	}
	$scope.createNewMember = function(id){
		/* this function return an empty member structure */
		var member = $scope.memberStructure();

		/* the new empty member is added to the list of the members */
		/* when the new member is added, new row is displayed in the browser */
        $scope.mark.members.push(member);

        var table = document.getElementsByClassName("table")[0];
		var tbody = table.getElementsByTagName("tbody")[0];
		
		/* this function creates the event save */
		function createEventSave(tr){
			var buttons = tr.querySelectorAll("button");
			var btnEdit = buttons[0];	
			var btnSave = buttons[1];

			/* the clases for the bottons are changed */
			btnSave.classList.add("save_member");
			btnEdit.setAttribute("class", "notShow");
			
			/* the image button changes to icon save */
			var img = btnSave.getElementsByTagName("img")[0];
			img.setAttribute("src", "/images/save.png");


			var btn = document.getElementsByClassName("save_member");
			var lastBtn = btn[btn.length - 1];

			lastBtn.addEventListener("click", updateMember, false);
		}

		function updateMember(e){
			var element = e.target.parentElement.parentElement;

			if(element.tagName === "TD"){
				element = element.parentElement;
			}
			$scope.trId = element.id;
			 
			var inputsValues = [];
			var memberUpdated = {};
			var isEmpty = false;
			var wasFound = false;
			
			inputsValues = $scope.getImputsValues($scope.trId);

			/* checks if the inputs are not empty */
			if(!$scope.memberHasEmptyValues(inputsValues[0],inputsValues[1])){
				memberUpdated = $scope.saveInputValuesToMemberObject(inputsValues,member);
				/* checks if the member that we want to create does't exits */
				/* if not exits the member is aded */
				wasFound = $scope.findMember(memberUpdated);

				if(!wasFound){
					$scope.mark.members[$scope.trId] = memberUpdated;
					var event = "save";
					$scope.UpdateJsonFile(event,element);
				}else{
					console.log("The member with that number already exits");
				}
			}else{
				console.log("Be sure that the name and the number have values");
			}
			
		}

		/* this setTimeout is to be sure that the new row is added */
		setTimeout(function(){

			var tr = tbody.lastElementChild;
			createEventSave(tr);

		},0);
	}

	$scope.memberStructure = function(){
		
		var member = {
            "name": "",
            "number": "",
            "ruta": "/employees/",
            "skype": {
                "user": ""
            },
            "social": {
                "facebook": "",
                "twitter": "",
                "linkedIn": "",
                "instagram": ""
            }
        }

        return member;
	}

	$scope.save = function(){
		
		console.log("dfsfsd");
	}

	$scope.editMember = function(member,id){
		var inputsValues = [];
		var memberUpdated = {};
		var isEmpty = false;
		var wasFound = false;
		
		inputsValues = $scope.getImputsValues(id);

		/* checks if the inputs are not empty */
		if(!$scope.memberHasEmptyValues(inputsValues[0],inputsValues[1])){
			memberUpdated = $scope.saveInputValuesToMemberObject(inputsValues,member);
			wasFound = $scope.findMember(memberUpdated);

			if(wasFound){
				$scope.mark.members[id] = memberUpdated;
				var event = "edit";
				$scope.UpdateJsonFile(event);
			}else{
				console.log("The member that you are trining to edit doesn't exist");
			}
		}else{
			console.log("Be sure that the name and the number have values");
		}
	}

	$scope.checkMemberBeforetoSave = function(member,id, isEditOrSave){
		
		var inputsValues = $scope.getImputsValues(id);
		console.log(inputsValues);
		var was_editted = false;

		/* this if if to check that the user wrote a number of the member*/
		if(inputsValues[0] !== "empty" && inputsValues[1] !== "empty"){

			var memberUpdated = $scope.saveInputValuesToMemberObject(inputsValues,member); 
			was_editted = $scope.findMember(memberUpdated);
			
			if(isEditOrSave === "save" && was_editted === false){
				return memberUpdated;
			}else{
				return memberUpdated;
			}
			
		}else{
			console.log("Be sure that the name and the number have values");
			was_editted = false;
		}
		return was_editted;
	}

 	/**
	* This function get an array with the values of the inputs
	*/	
	$scope.getImputsValues = function(id){
		
		var elemments = document.getElementById(id);
		// 
		var inputs = elemments.getElementsByTagName('input');
		
		var inputsValues = Object.keys(inputs).map(function function_name(x) {
			return inputs[x].value;
		});

		/* This function shift remove the first element of the array*/
		inputsValues.shift();

		return inputsValues
	}

	$scope.clone = function(obj){

		if ( obj === null || typeof obj  !== 'object' ) {
			return obj;
		}

		var temp = obj.constructor();
		
		for ( var key in obj ) {
			temp[ key ] = $scope.clone( obj[ key ] );
		}

		return temp;

	}
	/**
	* This function save the values in the member object member
	*/	
	$scope.saveInputValuesToMemberObject = function(inputsValues,member){
		var member2 = $scope.clone(member);
		// console.log(member);
		member2.name = inputsValues[0];
		member2.number = inputsValues[1];
		member2.skype.user = inputsValues[2];
		member2.social.facebook = inputsValues[3];
		member2.social.instagram = inputsValues[4];
		member2.social.linkedIn = inputsValues[5];
		member2.social.twitter = inputsValues[6];

		return member2;
	}

	/**
	* This function checks if the member exits in the valiable $scope.mark.members
	* that is a representation of the json file
	*/	
	$scope.findMember = function(memberUpdated){
		
		var members = $scope.mark.members;
		var found = false;

		for (var i = 0; i < members.length; i++) {
			
			if (members[i].number === memberUpdated.number) {
				
				found = true;
				break;
			}
		}
		return found;
	}

	/**
	* This function update the json file with the new information
	*/	
	$scope.UpdateJsonFile = function(event,element){

		/* this array has all the members that not have name and number empty*/
		$scope.members =[]

		function validateMembers(){
			// console.log($scope.mark);
			for (var i = 0; i < $scope.mark.members.length; i++) {
				
				/* checks if the value are not empty */
				if(!$scope.memberHasEmptyValues($scope.mark.members[i].name,$scope.mark.members[i].number)){
					$scope.members.push($scope.mark.members[i]);
				}
			}
		}
		validateMembers();

		/* creates a copy of the original object makr or team */
		$scope.object = $scope.clone($scope.mark);
		$scope.object.members = $scope.members;

		$http({
			method: 'POST',
			url: '/update_team/'+$scope.mark.name,
			data: $scope.object
			
		}).then(function successCallback(response) {
			console.log(element);
			if(event === "save"){
				var btnEdit = element.getElementsByClassName("notShow")[0];
				btnEdit.setAttribute("class", "btn");
				
				var btn = element.getElementsByClassName("save_member")[0];
				var img = btn.getElementsByTagName("img")[0];
				img.setAttribute("src", "/images/delete.png");

				btn.classList.toggle('save_member');
			}
			console.log("success")
		}, function errorCallback(response) {
			console.log("error");
		});
	}

	/* this function checks if the name and the number member are empty 
	* if one of them are empty returns true if not returns false
	*/
	$scope.memberHasEmptyValues = function(name,number){

		if((name === "empty" || name === "" ) || 
			(number === "empty" || number === "")){
			return true;
		}

		return false;
	}
}]);

app.controller('teamCollageController', ['$scope','$http', function($scope, $http) {
  	/**
	* This controller initialize the aplication
	*/
  	init();

	/**
	* The big-counts json is read and load all the information variable bigCountsList
	*/
  	$http.get('json/big-counts.json').then(function(data){
      	$scope.bigCountsList = data.data['Counts'];
    });

	/**
	* This function read the team json file and pass for parameter the big count that was selected
	*/
	$scope.showsCount = function(count){
		
		/**
		* The teams json is read and load all the information variable teams
		*/
		$http.get('json/teams.json').then(function(data){

	      	$scope.teams= data.data[count];
	   	});

		var mainDiv = document.getElementsByClassName('select-container')[0];
	    var divteams = mainDiv.getElementsByClassName('div-teams')[0];
	    divteams.classList.remove("width-zero");
	    var boderLightColor = divteams.getElementsByTagName('div')[0];	    
	    var selectTeam = divteams.getElementsByClassName('select-teams')[0];	    
		

		if(typeof(selectTeam) !== 'undefined')	{
	    	selectTeam.classList.remove("select-teams");
	    }

	    boderLightColor.classList.add("border-light-color");
	    boderLightColor.classList.add("showTeamsDropdown");
	}

	/**
	* This function read the specific team and load the information to the variable team
	*/
	$scope.changeTeam = function(teamSelected){
		var testTeam;
	    $http.get('json/teams/'+teamSelected+'.json').then(function(data){
	      	$scope.team = data.data;
	    });
		hideWelcomePage()
	}

}]);

/**
* This directive is used to change the photo of the member team 
* if the path of the original photo is broken
*/
app.directive('fallbackSrc', function () {
	var fallbackSrc = {
		link: function postLink(scope, iElement, iAttrs) {
			iElement.bind('error', function() {
				angular.element(this).attr("src", iAttrs.fallbackSrc);
			});
		}
	}
	return fallbackSrc;
});

function hideWelcomePage(){
	 var _tt = new TimelineMax();
     _tt
        .add("go")
	 	.to('.div-logo',.2,{css:{display:"block"}},"go")
		.to('body', .7, { y: -310 },"go")
		.to('.inital-conatiner', 1, { boxShadow: "0 8px 8px" },"go")
		//.from('.gallery-container', .5, { opacity:0 })
}
