
var init = function() {
    initAnimations();
}

function initAnimations() {
    var _tt = new TimelineMax();
    _tt.set('.select-container',{ css:{display:""}})
        .from('#prodigious-logo-1', 1, { opacity: 0, scale: 0 }, 0)
        .from('#prodigious-logo-1', 3, { rotation: 360, transformOrigin: "50% 50%", repeat: -1, ease:Linear.easeNone }, 0)
        
        .to('#welcome-text-1', 0, { className:"-=hide" }, .5)
        .from('#welcome-text-1', 1, { opacity: 0 }, .5)
        .to('#welcome-text-1', 1, { opacity: 0 }, 2)
        .to('#welcome-text-1', 0, { className:"+=hide" }, 3)

        .to('#welcome-text-2', 0, { className:"-=hide" }, 3)
        .from('#welcome-text-2', 1, { opacity: 0 }, 3)
        .to('#welcome-text-2', 1, { opacity: 0 }, 5)
        .to('#welcome-text-2', 0, { className:"+=hide" }, 6)
        
        .to('#prodigious-logo-1', 1.5, { x:'-130%',y:'-22%',scale:.65 }, 5)
        .to('#prodigious-logo-1', 3, { rotation: 0, transformOrigin: "50% 50%" }, 4)
        
        .to('#prodigious-logo-2', 0, { className:"-=hide" }, 7)
        .from('#prodigious-logo-2', 1, { opacity:0 }, 7)

        .to('#welcome-text-3', 0, { className:"-=hide" }, 8)
        .from('#welcome-text-3', 1, { opacity: 0 }, 8)
        // .set('.select-container',{ css:{display:"block"}},8.5)
        // .from('.select-container', .5,{ css:{opacity:0}},8.5)
}
