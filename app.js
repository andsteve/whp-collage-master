var express = require('express');
var livereload = require('express-livereload');
var bodyParser = require('body-parser');
var app = express();

// var jsonfile = require('jsonfile');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());


livereload(app, config={});
app.use(express.static(__dirname + '/files/site'));
app.use(require('connect-livereload')({
    port: "3000"
}));

/**
* this route is for update a specific json team selected
*/
app.post('/update_team/:mark', function (req, res) {

	var jsonfile = require('jsonfile');
	 
	var file = 'files/site/json/teams/'+req.params.mark+'.json';
	var obj = req.body;

	// console.log(res);
	 
	jsonfile.writeFile(file, obj,{spaces: 4}, function (err) {
	 console.error(err);
	   	if(err) {
		 	return console.error(err);
		}
		res.send('The file was saved!');
	});
	// .then(function successCallback(response) {
	// 		console.log("success");
	// 	});
});


app.listen(process.env.PORT || 3000);


